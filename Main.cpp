#include <iostream>
#include <iomanip>
#include <time.h>

int main()
{
	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);

	const int RazmerMassiva = 10;
	int ArrayIndexSum[RazmerMassiva][RazmerMassiva];
	int SummaStroki = 0;
	
	//���� ��� �������� � ������ �������
	for (int i = 0; i < RazmerMassiva; i++)
	{
		for (int j = 0; j < RazmerMassiva; j++)
		{
			ArrayIndexSum[i][j] = i + j;
			std::cout << std::setw(4) << ArrayIndexSum[i][j];
			if ((buf.tm_mday % RazmerMassiva) == i)
			{
				SummaStroki += ArrayIndexSum[i][j];
			}
		}
		std::cout << '\n';
	}
	std::cout << SummaStroki;

}